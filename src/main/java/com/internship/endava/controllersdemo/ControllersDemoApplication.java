package com.internship.endava.controllersdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControllersDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControllersDemoApplication.class, args);
	}

}
